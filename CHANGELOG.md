# CHANGELOG

<!--- next entry here -->

## 0.1.0
2020-01-29

### Features

- **semver:** set new version feature (307fc88ee82457ba57499cbd6d02ed7a5de1f897)

### Fixes

- **add ci:** ci pipeline (7e7790a3ffed6593c30a1d46c864d3d81ce6d3df)
- **ci:** fix from ci gitalb (c48ca95b3ab5c8a77c8cfeddc08841aaeb954fcb)

