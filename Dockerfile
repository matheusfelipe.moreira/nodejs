FROM node:10

# Create app directory
WORKDIR /usr/src/app

# VARS FROM VERSION 
ARG TAG_COMMIT=1.0.0

# Install app dependencies
# A wildcard is used to ensure both package.json AND package-lock.json are copied
# where available (npm@5+)
COPY app/package*.json ./

RUN npm install
RUN npm version ${TAG_COMMIT}
# If you are building your code for production
# RUN npm ci --only=production

# Bundle app source
COPY app/. .

EXPOSE 3000
CMD [ "node", "app.js" ]